<div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="index.blade.php">MASTER KOI</a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">
                    <li class="active">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-dashboard"></i><span>dashboard</span></a>
                    </li>

                    <li>
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-pie-chart"></i><span>Charts</span></a>
                    </li>

                    <li>
                        <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-table"></i>
                            <span>Tables</span></a>
                        <ul class="collapse">
                            <li><a href="table-basic.html">basic table</a></li>
                            <li><a href="table-layout.html">table layout</a></li>
                            <li><a href="datatable.html">datatable</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-exclamation-triangle"></i>
                            <span>Error</span></a>
                        <ul class="collapse">
                            <li><a href="404.html">Error 404</a></li>
                            <li><a href="403.html">Error 403</a></li>
                            <li><a href="500.html">Error 500</a></li>
                        </ul>
                    </li>

                </ul>
            </nav>
        </div>
    </div>
</div>